from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import generate_config_static


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_fail2ban/django_cdstack_tpl_fail2ban"

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = list()

    if "apt_packages" in template_opts:
        template_opts["fail2ban_apps"] += template_opts["apt_packages"]

    # remove duplicates
    template_opts["fail2ban_apps"] = list(dict.fromkeys(template_opts["fail2ban_apps"]))

    generate_config_static(zipfile_handler, template_opts, module_prefix)

    return True
