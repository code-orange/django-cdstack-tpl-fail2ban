#!/bin/sh

# Touch log files
{% if '3proxy' in fail2ban_apps %}
touch /var/log/3proxy.log
{% endif %}

{% if 'apache-auth' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-overflows' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-badbots' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-nohome' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-botsearch' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-fakegooglebot' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-modsecurity' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'apache-shellshock' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps %}
mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'assp' in fail2ban_apps %}

{% endif %}

{% if 'asterisk' in fail2ban_apps %}
mkdir -p /var/log/asterisk
touch /var/log/asterisk/messages
touch /var/log/asterisk/security
{% endif %}

{% if 'bitwarden' in fail2ban_apps %}

{% endif %}

{% if 'centreon' in fail2ban_apps or 'centreon-broker' in fail2ban_apps %}
mkdir -p /var/log/centreon
touch /var/log/centreon/login.log
{% endif %}

{% if 'courier-smtp' in fail2ban_apps or 'courier-mta' in fail2ban_apps %}

{% endif %}

{% if 'courier-auth' in fail2ban_apps or 'courier-mta' in fail2ban_apps %}

{% endif %}

{% if 'cyrus-imap' in fail2ban_apps or 'cyrus-imapd' in fail2ban_apps %}

{% endif %}

{% if 'directadmin' in fail2ban_apps %}
mkdir -p /var/log/directadmin
touch /var/log/directadmin/login.log
{% endif %}

{% if 'dovecot' in fail2ban_apps or 'dovecot-imapd' in fail2ban_apps or 'dovecot-pop3d' in fail2ban_apps or 'dovecot-lmtpd' in fail2ban_apps %}
touch /var/log/mail.log
{% endif %}

{% if 'drupal-auth' in fail2ban_apps %}

{% endif %}

{% if 'ejabberd-auth' in fail2ban_apps or 'ejabberd' in fail2ban_apps %}
mkdir -p /var/log/ejabberd
touch /var/log/ejabberd/ejabberd.log
{% endif %}

{% if 'exim' in fail2ban_apps or 'exim4' in fail2ban_apps or 'exim4-base' in fail2ban_apps %}
mkdir -p /var/log/exim
touch /var/log/exim/main.log
{% endif %}

{% if 'exim-spam' in fail2ban_apps or 'exim' in fail2ban_apps or 'exim4' in fail2ban_apps or 'exim4-base' in fail2ban_apps %}
mkdir -p /var/log/exim
touch /var/log/exim/main.log
{% endif %}

{% if 'freeswitch' in fail2ban_apps or 'freeswitch-meta-all' in fail2ban_apps %}
mkdir -p /var/log/freeswitch
touch /var/log/freeswitch/freeswitch.log
{% endif %}

{% if 'froxlor-auth' in fail2ban_apps or 'froxlor' in fail2ban_apps %}

{% endif %}

{% if 'groupoffice' in fail2ban_apps %}
mkdir -p /home/groupoffice/log
touch /home/groupoffice/log/info.log
{% endif %}

{% if 'gssftpd' in fail2ban_apps %}

{% endif %}

{% if 'guacamole' in fail2ban_apps %}
if [ -d "/var/log/tomcat6" ]; then
  mkdir -p /var/log/tomcat6
  touch /var/log/tomcat6/catalina.out
fi

if [ -d "/var/log/tomcat7" ]; then
  mkdir -p /var/log/tomcat7
  touch /var/log/tomcat7/catalina.out
fi

if [ -d "/var/log/tomcat8" ]; then
  mkdir -p /var/log/tomcat8
  touch /var/log/tomcat8/catalina.out
fi

if [ -d "/var/log/tomcat9" ]; then
  mkdir -p /var/log/tomcat9
  touch /var/log/tomcat9/catalina.out
fi

if [ -d "/var/log/tomcat10" ]; then
  mkdir -p /var/log/tomcat10
  touch /var/log/tomcat10/catalina.out
fi
{% endif %}

{% if 'haproxy-http-auth' in fail2ban_apps or 'haproxy' in fail2ban_apps %}
touch /var/log/haproxy.log
{% endif %}

{% if 'horde' in fail2ban_apps or 'php-horde-webmail' in fail2ban_apps %}
mkdir -p /var/log/horde
touch /var/log/horde/horde.log
{% endif %}

{% if 'kerio' in fail2ban_apps %}
mkdir -p /opt/kerio/mailserver/store/logs
touch /opt/kerio/mailserver/store/logs/security.log
{% endif %}

{% if 'lighttpd-auth' in fail2ban_apps or 'lighttpd' in fail2ban_apps %}
mkdir -p /var/log/lighttpd
touch /var/log/lighttpd/error.log
{% endif %}

{% if 'mongodb-auth' in fail2ban_apps or 'mongodb' in fail2ban_apps or 'mongodb-org' in fail2ban_apps or 'mongodb-org-server' in fail2ban_apps %}
mkdir -p /var/log/mongodb
touch /var/log/mongodb/mongodb.log
{% endif %}

{% if 'monit' in fail2ban_apps %}
touch /var/log/monit
touch /var/log/monit.log
{% endif %}

{% if 'murmur' in fail2ban_apps or 'mumble-server' in fail2ban_apps %}

{% endif %}

{% if 'mysqld-auth' in fail2ban_apps or 'mariadb-server' in fail2ban_apps or 'mysql-server' in fail2ban_apps %}
mkdir -p /var/log/mariadb
touch /var/log/mariadb/mariadb.log
touch /var/log/mysqld.log
{% endif %}

{% if 'nagios' in fail2ban_apps or 'nagios4' in fail2ban_apps or 'nagiosxi' in fail2ban_apps %}

{% endif %}

{% if 'named-refused' in fail2ban_apps or 'bind9' in fail2ban_apps %}
mkdir -p /var/log/named
touch /var/log/named/security.log
{% endif %}

{% if 'nginx-http-auth' in fail2ban_apps or 'nginx' in fail2ban_apps or 'nginx-full' in fail2ban_apps %}
mkdir -p /var/log/nginx
touch /var/log/nginx/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'nginx-limit-req' in fail2ban_apps or 'nginx' in fail2ban_apps or 'nginx-full' in fail2ban_apps %}
mkdir -p /var/log/nginx
touch /var/log/nginx/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'nginx-botsearch' in fail2ban_apps or 'nginx' in fail2ban_apps or 'nginx-full' in fail2ban_apps %}
mkdir -p /var/log/nginx
touch /var/log/nginx/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'nsd' in fail2ban_apps %}
touch /var/log/nsd.log
{% endif %}

{% if 'openwebmail' in fail2ban_apps %}
touch /var/log/openwebmail.log
{% endif %}

{% if 'oracleims' in fail2ban_apps %}
mkdir -p /opt/sun/comms/messaging64/log
touch /opt/sun/comms/messaging64/log/mail.log_current
{% endif %}

{% if 'perdition' in fail2ban_apps %}

{% endif %}

{% if 'phpmyadmin-syslog' in fail2ban_apps or 'phpmyadmin' in fail2ban_apps %}

{% endif %}

{% if 'php-url-fopen' in fail2ban_apps or 'apache' in fail2ban_apps or 'apache2' in fail2ban_apps or 'nginx' in fail2ban_apps or 'nginx-full' %}
mkdir -p /var/log/nginx
touch /var/log/nginx/error.log

mkdir -p /var/log/apache2
touch /var/log/apache2/access.log
touch /var/log/apache2/error.log

mkdir -p /var/www/web-dummy/log
touch /var/www/web-dummy/log/access.log
touch /var/www/web-dummy/log/error.log
{% endif %}

{% if 'portsentry' in fail2ban_apps %}
mkdir -p /var/lib/portsentry
touch /var/lib/portsentry/portsentry.history
{% endif %}

{% if 'postfix' in fail2ban_apps %}
touch /var/log/mail.log
{% endif %}

{% if 'postfix-sasl' in fail2ban_apps or 'libsasl2-modules' in fail2ban_apps %}
touch /var/log/mail.log
{% endif %}

{% if 'proftpd' in fail2ban_apps %}
mkdir -p /var/log/proftpd
touch /var/log/proftpd/proftpd.log
touch /var/log/proftpd/auth.log
{% endif %}

{% if 'pure-ftpd' in fail2ban_apps or 'pure-ftpd-mysql' in fail2ban_apps %}
touch /var/log/syslog
{% endif %}

{% if 'qmail-rbl' in fail2ban_apps or 'qmail' in fail2ban_apps %}
mkdir -p /service/qmail/log/main
touch /service/qmail/log/main/current
{% endif %}

{% if 'roundcube-auth' in fail2ban_apps or 'roundcube' in fail2ban_apps %}
mkdir -p /var/log/roundcube
touch /var/log/roundcube/errors
{% endif %}

{% if 'screensharingd' in fail2ban_apps %}
touch /var/log/system.log
{% endif %}

{% if 'sendmail-auth' in fail2ban_apps or 'sendmail' in fail2ban_apps %}

{% endif %}

{% if 'sendmail-reject' in fail2ban_apps or 'sendmail' in fail2ban_apps %}

{% endif %}

{% if 'sieve' in fail2ban_apps or 'dovecot-sieve' in fail2ban_apps %}

{% endif %}

{% if 'slapd' in fail2ban_apps %}
touch /var/log/slapd.log
{% endif %}

{% if 'sogo-auth' in fail2ban_apps or 'sogo' in fail2ban_apps %}

{% endif %}

{% if 'solid-pop3d' in fail2ban_apps %}

{% endif %}

{% if 'squid' in fail2ban_apps %}
mkdir -p /var/log/squid
touch /var/log/squid/access.log
{% endif %}

{% if 'squirrelmail' in fail2ban_apps %}
mkdir -p /var/lib/squirrelmail/prefs
touch /var/lib/squirrelmail/prefs/squirrelmail_access_log
{% endif %}

{% if 'tine20' in fail2ban_apps %}
mkdir -p /var/log/tine20
touch /var/log/tine20/tine20.log
{% endif %}

{% if 'traefik-auth' in fail2ban_apps or 'traefik' in fail2ban_apps %}
mkdir -p /var/log/traefik
touch /var/log/traefik/access.log
{% endif %}

{% if 'uwimap-auth' in fail2ban_apps or 'uw-imap' in fail2ban_apps %}

{% endif %}

{% if 'vsftpd' in fail2ban_apps %}
touch /var/log/vsftpd.log
{% endif %}

{% if 'webmin-auth' in fail2ban_apps or 'webmin' in fail2ban_apps %}

{% endif %}

{% if 'wuftpd' in fail2ban_apps or 'wu-ftpd' in fail2ban_apps %}

{% endif %}

{% if 'znc-adminlog' in fail2ban_apps or 'znc' in fail2ban_apps %}
mkdir -p /var/lib/znc/moddata/adminlog
touch /var/lib/znc/moddata/adminlog/znc.log
{% endif %}

{% if 'zoneminder' in fail2ban_apps %}

{% endif %}
